+++
title = "Weekly GNU-like Mobile Linux Update (49/2022): Fedora 38 Mobility Phosh Spin coming and Plasma Gear 22.12"
date = "2022-12-12T12:29:13Z"
draft = false
[taxonomies]
tags = ["Nemo Mobile","Fedora", "Librem 5 Camera", "Plasma Gear",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "Peter (with friendly assistance from plata's awesome script)"
+++

Also: Nemo Mobile is all in on #adventofcode, postmarketOS 22.12 is coming closer, autofocus coming to the Librem 5 camera.
<!-- more -->

_Commentary in italics._


### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#73 Removing Autotools](https://thisweek.gnome.org/posts/2022/12/twig-73/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: New Spectacle](https://pointieststick.com/2022/12/09/this-week-in-kde-new-spectacle/)
- Volker Krause: [KDE Frameworks 6 Branching](https://www.volkerkrause.eu/2022/12/10/kf6-branching-update.html)
- KDE Announcements: [KDE Ships Frameworks 5.101.0](https://kde.org/announcements/frameworks/5/5.101.0/)
- KDE Announcements: [KDE Gear 22.12 is Here!](https://kde.org/announcements/gear/22.12.0/)
- Nate Graham: [Status of the 15-Minute Bug Initiative](https://pointieststick.com/2022/12/04/status-of-the-15-minute-bug-initiative/)

#### Nemo Mobile
- While things are generally winding down a bit towards the end of the year, Nemo Mobile are doing the #adventofcode thing. [Make sure to follow Jozef Mlich on the Fediverse!](https://fosstodon.org/@jmlich)

#### Ubuntu Touch
- [Greetings from the UBports Newsletter Team | Ubuntu Touch](https://ubuntu-touch.io/en/blog/from-the-ubports-newsletter-team-15/post/greetings-from-the-ubports-newsletter-team-3869)

#### Distributions
- Phoronix: [Fedora 38 Cleared To Produce "Mobility Phosh" Spins](https://www.phoronix.com/news/Fedora-Mobility-Phosh-Approved)
- PinePhone (Reddit): [Kali 2022.4 officially supports Pine phone hardware...](https://www.reddit.com/r/pinephone/comments/zf50qo/kali_20224_officially_supports_pine_phone_hardware/)

#### Stack
- Phoronix: [PipeWire 0.3.62 Released With Bluetooth Offloading Support](https://www.phoronix.com/news/PipeWire-0.3.62)
- Phoronix: [Meson 1.0 Build System Nears With Stable Rust Module, Other Improvements](https://www.phoronix.com/news/Meson-1.0-RC1)
- Phoronix: [Mesa 22.2.5 Released To End Out The Series](https://www.phoronix.com/news/Mesa-22.2.5-Released)

#### Linux 
- Phoronix: [The Most Interesting New Features For Linux 6.1](https://www.phoronix.com/news/Linux-6.1-Features)
- Phoronix: [Linux 6.1 Released With MGLRU, Initial Rust Code](https://www.phoronix.com/news/Linux-6.1-Released)

#### Non-Linux

#### Matrix
- Matrix.org: [This Week in Matrix 2022-12-09](https://matrix.org/blog/2022/12/09/this-week-in-matrix-2022-12-09)
- Matrix.org: [Synapse 1.73 released](https://matrix.org/blog/2022/12/07/synapse-1-73-released)

### Worth noting
- u/GuyWithMalePronouns: [Librem 5 first impressions; comparison to Pinephones : Purism](https://teddit.net/r/Purism/comments/zh2xlx/librem_5_first_impressions_comparison_to/)
- [Autofocus in millipixels](https://forums.puri.sm/t/autofocus-in-millipixels/18925)
- [Must resist and not buy banana phone!](https://fosstodon.org/@martijnbraam/109446834493561457)
- [hamblingreen: "#sxmo tip](https://fosstodon.org/@hamblingreen/109458344269997953)
- [Guido Günther: "📱 Device maintainers and #phosh users (hopefully) rejoice: #feedbackd will soon support merging themes so you can e.g. chain up to the default theme and just replace the feedback for events you want to change. There's also a validator to check your changes (which is also used in CI).](https://social.librem.one/@agx/109472459346879014)

### Worth reading
- Chris Vogel: [#MonkeyIsland on #Librem5 and #Pinephone](https://chrichri.ween.de/articles/9123ba8/monkeyisland-on-librem5-and-pinephone)

### Worth listening
- postmarketOS Podcast: [#26 Mastodon, Mailbag, v22.12, RISCV, pmbootstrap CI](https://cast.postmarketos.org/episode/26-Mastodon-Mailbag-v22.12-RISCV-pmb-CI/). _Great episode, make sure to give it a listen!_

### Worth watching
- Bitter Epic Productions: [PinePhone Pro Hardware Update! The Camera! The Keyboard! The Wireless Charging Case!](https://www.youtube.com/watch?v=LSFtYgg6ocw)
- CC Media: [Install Ubuntu Touch Poco M3 or Any Devices](https://www.youtube.com/watch?v=3hXOxhGBjPg)
- Ubuntu OnAir: [Ubuntu Summit 2022 | How we built open source Raspberry Pi tablets and survived the chipset shortage](https://www.youtube.com/watch?v=24P7KmgQV14)
- The Linux Experiment: [That's what a LINUX TABLET is supposed to look like! FydeTab Duo](https://www.youtube.com/watch?v=uCa-8isUxno)
- ETA PRIME: [FydeTab Duo Hands-On, An All-New Open-Source Arm-Based Hackable Linux Tablet!](https://www.youtube.com/watch?v=sG8AOnSeaSE)

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__


PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

