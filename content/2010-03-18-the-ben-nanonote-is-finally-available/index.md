+++
title = "The Ben NanoNote is finally available!"
aliases = ["2010/03/ben-nanonote-is-finally-available.html"]
date = "2010-03-18T13:22:00Z"
[taxonomies]
tags = ["Ben NanoNote", "copyleft hardware", "NanoNote", "openness", "Qi Hardware"]
categories = ["hardware"]
authors = ["peter"]
+++
It has been a long while since the first announcement, but apparently the 1st generation NanoNote, part of the <a href="http://en.qi-hardware.com/wiki/Main_Page">Qi Hardware project</a>, is available now (at least in the <a href="http://www.tuxbrain.com/en/oscommerce/products/68">European Union</a> and the <a href="http://sharism.cc/shop/">US</a>).
<!-- more -->
<a href="nano-open_sm.jpg"><img style="float:right;margin:10px 0 10px 10px;cursor:pointer;cursor:hand;width: 320px;height: 214px" src="nano-open_sm.jpg" border="0" alt="Ben NanoNote" /></a>

Looking at the specs might disappoint you (especially the 3,0&#8221; QVGA, 32MB Ram, USB Device only part), but you have to keep in mind that it is still the _&#8220;most advanced piece of copyleft hardware&#8221;_ and it is just $ 99&mdash;which translates to 99€ (incl. VAT).

I still don't know whether I will get a NanoNote&mdash;while I generally like the clamshell formfactor, I doubt it would be useful to me&mdash;I suck at (low level) programming and I have more than one smartphone that runs Linux.

But that shouldn't stop you :-) 

Interested? Make sure to watch this video:
* <a href="https://www.dailymotion.com/video/xbd1uy">tuxbrain: Playing FreeDoom on NanoNote</a>.
