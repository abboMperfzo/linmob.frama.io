+++
title = "Looking forward to #FOSDEM2023"
date = "2023-02-03T14:40:00Z"
updated = "2023-02-12T20:15:00Z"
draft = false
[taxonomies]
tags = ["FOSDEM 2023", "talks", "camera"]
categories = ["personal", "events",]
authors = ["peter"]
[extra]
edit_post_with_images = true
update_note = "Added comments on how the talks were."
+++

With [FOSDEM](https://fosdem.org/), a free event for software developers to meet, share ideas and collaborate in Brussels, Belgium, approaching (it's this weekend, 4 & 5 February 2023), let's have a little preview on the talks that are particularly exciting for #linuxmobile.
<!-- more -->

### The ["FOSS on Mobile Devices"](https://fosdem.org/2023/schedule/track/foss_on_mobile_devices/) Track/Developer Room

Just like last year, where FOSDEM was happening virtually, there's a specific room for the topic this blog is mostly about.

All talks of the track happen in Room [UB 4.136](https://fosdem.org/2023/schedule/room/ub4136/) on Saturday, February 4th from 10:30 to 14:30. Every talk in this track is 15 to 25 minutes long.

Make sure to check the Talk pages for links to chat rooms (Matrix) and live streams!

#### Lomiri Mobile Linux in Desktop mode by Alfred Neumayer (10:30)

Convergence on Ubuntu Touch is definitely interesting, and a lot of work went into this since UBports took over from Canonical. Not having tried Convergence with Ubuntu Touch in years (I think I tried on a Nexus 4 (?) using a Slimport adapter years ago), I am quite excited in getting a demo and a deep dive into what changed since.

* [Talk page](https://fosdem.org/2023/schedule/event/lomiri/)

_Alfred arrived about 3 minutes before the talk started - and used the Fairphone 4 with Ubuntu Touch to do the presentation. Absolutely amazing!_

#### Sharp photos and short movies on a mobile phone by Pavel Machek (11:00)

You may recall my toots about Librem 5 camera improvements, and Pavel Machek contributed to this in a major way. Aside from the Librem 5, the work on PinePhone 2MP 30FPS video recording is definitely interesting.

* [Talk page](https://fosdem.org/2023/schedule/event/sharp_photos/)
* See also: [More camera talks](https://linmob.net/looking-forward-to-fosdem/#convergent-camera-applications-for-mobile-linux-devices-by-kieran-bingham-13-30)

_Great, really fun talk on the difficulties of getting usable images out of a sensor. Must watch!_

#### Mainline Linux on recent Qualcomm SoCs: Fairphone 4 by Luca Weiss (11:30)

15 minutes of porting mainline to the Snapdragon 750G based, 5G Fairphone 4.

* [Talk page](https://fosdem.org/2023/schedule/event/mainline_on_the_fairphone4/)
* [Slides](https://fosdem.org/2023/schedule/event/mainline_on_the_fairphone4/attachments/slides/5454/export/events/attachments/mainline_on_the_fairphone4/slides/5454/Mainline_Linux_on_recent_Qualcomm_SoCs_Fairphone_4.pdf)

_Mainlining is quite the endeavour. Listen to this, if you think 'it can't be that hard'._

#### Mobian: to stable ... and beyond! by Arnaud Ferraris (11:50)

Mobian will move to stable after the release of Debian 12 Bookworm - thrilled to hear about the details!

* [Talk page](https://fosdem.org/2023/schedule/event/mobian_to_stable_and_beyond/)
* [Slides](https://fosdem.org/2023/schedule/event/mobian_to_stable_and_beyond/attachments/slides/5472/export/events/attachments/mobian_to_stable_and_beyond/slides/5472/Mobian_FOSDEM23.pdf)

_Moving to stable is going to be great for users!_

#### What's new in the world of phosh? by Evangelos Ribeiro Tzaras (12:10)

A lot happened since February 2022 :)

* [Talk page](https://fosdem.org/2023/schedule/event/phosh/)

_Nice talk about all the improvements implemented._

#### Ondev2: Distro-Independent Installer For Linux Mobile	by Oliver Smith (12:30)

Ollie (of postmarketOS fame) told me about this a while back, but I have not seen it yet! (If you're impatient and can't wait until tomorrow: [Source code](https://gitlab.com/postmarketOS/ondev2))

* [Talk page](https://fosdem.org/2023/schedule/event/ondev2_installer/)

_It's going to be a lot better than the current solution, awesome!_

#### Sailing into the Linux port with Sony Open Devices by Björn Bidar (12:50)

Porting SailfishOS to Sony devices.
* [Talk page](https://fosdem.org/2023/schedule/event/sailfish/)

_Sony Open Devices offers a middle-ground between being stuck on a vendor kernel forever and mainlining._

#### Writing a convergent application in 2023 with Kirigami by Carl Schwan (13:10)

Quite a task for 25 minutes!

* [Talk page](https://fosdem.org/2023/schedule/event/convergent_kirigami_apps/)

_Good explainer on Kirigami!_

#### Can Genode on the PinePhone question the notion of a smartphone? by Norman Feske (13:40)

Not Linux, but I really look forward to this!

* [Talk page](https://fosdem.org/2023/schedule/event/genode_on_the_pinephone/)

_This was really impressive. Make sure to [try it yourself](https://genodians.org/nfeske/2023-02-01-mobile-sculpt), if you own a 3GB PinePhone._

#### Where do we go from here? by Clayton Craft (14:10)

Lately, I have adopted a slightly bleak outlook on the future of Linux on Mobile - I hope Clayton can change my mind!

* [Talk page](https://fosdem.org/2023/schedule/event/future_of_mobile/)

_Nice closing talk! Let's hope we manage to collectively find ways to reduce duplicate efforts and manage to work towards better usability together._

### Track [Embedded, Mobile and Automotive](https://fosdem.org/2023/schedule/track/embedded_mobile_and_automotive/).

* [Room UD2.120](https://fosdem.org/2023/schedule/room/ud2120_chavanne/).


#### Convergent camera applications for mobile Linux devices by Kieran Bingham (13:30)

libcamera on the PinePhone Pro, whoohoo!
_Totally exciting, but I definitely won't make it, since I'll just stick to the track above._

* [Talk page](https://fosdem.org/2023/schedule/event/linux_camera_apps/)

#### Advanced Camera Support on Allwinner SoCs with Mainline Linux by Paul Kocialkowski (14:00)

Might be interesting if you are considering further PinePhone or PineCube development.

* [Talk page](https://fosdem.org/2023/schedule/event/allwinner_camera/)

#### Bluetooth state in PipeWire and WirePlumber by Frédéric Danis (17:05)

* [Talk page](https://fosdem.org/2023/schedule/event/bt_pipewire/)

#### Implementing VoLTE support for FOSS on mobile devices, by Marius Gripsgard (18:00)

* [Talk page](https://fosdem.org/2023/schedule/event/foss_volte/)

### Other talks (Saturday, February 4th)

#### Modern Camera Handling in Chromium by Michael Olbrich (Saturday, 11:30)

* [Talk page](https://fosdem.org/2023/schedule/event/om_chromium/)
* Track: [Open Media devroom](https://fosdem.org/2023/schedule/track/open_media/), [Room K.3.401](https://fosdem.org/2023/schedule/room/k3401/)

### Other talks (Sundey, February 5th)

#### (Keynote) What could go wrong? Me, I was by Richard Brown (9:00)

Subtitle: Containerised Applications are the way

* [Talk page](https://fosdem.org/2023/schedule/event/containerised_apps/)


Of course, there's a lot more, make sure to browse the full schedule [on the Web](https://fosdem.org/2023/schedule/) or in an app like [Kongress](https://linuxphoneapps.org/apps/org.kde.kongress/) or [Confy](https://linuxphoneapps.org/apps/net.kirgroup.confy/).

_Did I miss a relevant talk? Please get in touch and educate me :D_

### Stands

Now let's continue with what's mostly interesting for those who can make it to Brussels like me. [Stands](https://fosdem.org/2023/stands/), where projects can showcase their stuff!

* Linux on Mobile (Sailfish OS, Ubuntu Touch, postmarketOS, Mobian and more) was supposed to be in building H on Level 1, but I've been told actually is in building K on level 2. _I may hang out there a lot._

* PINE64 is in building AW on Level 1 (with Manjaro) _I'll certainly visit to catch a glimpse at PineTab2._

And that brings me to the important point: The hallway track, talking to people - that's what makes this better than a virtual conference.

### Let's meet and get a sticker

![Three LINMOB stickers, shot on Librem 5](stickers.png)

I'll be there, and I somehow managed to get some stickers printed, which I'll happily hand out :D

