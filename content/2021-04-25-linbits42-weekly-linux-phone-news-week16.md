+++
title = "LinBits 42: Weekly Linux Phone news / media roundup (week 16)"
aliases = ["2021/04/25/linbits41-weekly-linux-phone-news-week16.html", "2021/04/25/linbits42-weekly-linux-phone-news-week16.html"]
layout = "post"
date = "2021-04-25T21:34:00Z"
author = "Peter"
[taxonomies]
authors = ["peter"]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "SailfishOS", "Geary", "PineTalk", "Maemo Leste", "Fractal", "Maui", "CoreStuff", "JingOS", "JingPad", "Flatpak", "Flatseal",]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

PineTime, Maui and Geary hit milestones, a first PinePhone Beta Edition Unboxing and more! _Commentary in italics._
<!-- more -->


### Software development and releases
* [Geary 40 has been released](https://floss.social/@geary/106120094514884022). Among the new features is added support for half-screen, portrait and small displays. _I tested the flatpak, and yes: Seperate Geary packaging for mobile seems to be no longer necessary. Awesome!_ 


### Worth noting
* Joao: [Megapixels running on the Librem 5](https://social.librem.one/@joao/106102675899826772). _I tried this on my Librem 5 and wasn't successful on PureOS Byzantium._ 
* Luke: [PinePhone Keyboard update [April 24/04]](https://forum.pine64.org/showthread.php?tid=13684). _I am really looking forward to this!_

### Worth reading 

* Maemo Leste: [Funding from NGI EU](https://maemo-leste.github.io/funding-from-ngi-eu.html). _Great to see them receiving funding for their quite ambitious plans!_
* NxOS: [Maui Weekly Report 10](https://nxos.org/maui/maui-weekly-report-10/). _Great updates!_

#### Software corner
* Julian's Code Corner: [Getting Fractal up to speed](https://blogs.gnome.org/jsparber/2021/04/23/getting-fractal-up-to-speed/). _Looking forward for the next release of Fractal._
* Martín Abente Lahaye: [Flatseal 1.7.0](https://blogs.gnome.org/tchx84/2021/04/23/flatseal-1-7-0/). _Flatseal is a great tool, and this is a massive update!_
* Purism: [Purism and Linux 5.12](https://puri.sm/posts/purism-and-linux-5-12/). _I love upstream work!_
* kath-leinir: [Peruse 2.0 Beta 1 "The Long Awaited Beta"](https://kath-leinir.blogspot.com/2021/04/peruse-20-beta-1-long-awaited-beta.html). _If you are into comic books, check this one out!_
* Gamey: [Best web browsers for the Pinephone!](https://odysee.com/@gamey:c/pinephone-browsers:e). _If you are new to the PinePhone, this should be really helpful!_

#### Hardware corner
* PINE64: [It's Time: InfiniTime 1.0](https://www.pine64.org/2021/04/22/its-time-infinitime-1-0/). _The PineTime is getting there, and I am getting one or three._
* Linux Smartphones: [PinePhone keyboard prototype transforms the Linux phone into a tiny Linux laptop](https://linuxsmartphones.com/pinephone-keyboard-prototype-transforms-the-linux-phone-into-a-tiny-linux-laptop/). _While the design introduces tradeoffs regarding e.g. telephony, I am very much looking forward to it!_
* Linux Smartphones: [Sony Xperia 10 II will be able to run Sailfish OS soon](https://linuxsmartphones.com/sony-xperia-10-ii-will-be-able-to-run-sailfish-os-soon/). _Noteworthy: It's the first phone to run the 64-bit version of SailfishOS._

#### Tutorial corner
* Danct12: [VNC on PinePhone](https://danct12.github.io/VNC-on-PinePhone/). _Nice WayVNC tutorial._
* PINE64 Wiki: [PinePhone MMS with Matrix](https://wiki.pine64.org/wiki/PinePhone_MMS_with_Matrix). _This explains how to get MMS working over a local Matrix server. See this as a stop-gap solution, with Chatty getting better Matrix support soon this will at least get a bit more simple._
* beltrandroid: [Tutorial: Full disk encryption on Librem5](https://forums.puri.sm/t/tutorial-full-disk-encryption-on-librem5/13189)


### Worth listening
* PineTalk 007: [My Name is 64 ... PINE64](https://www.pine64.org/podcast/007-my-name-is-64-pine64/). _The second interview, this time with Danct12! Give it a listen!_

### Worth watching
* mutantC: [CoreStuff in postmarketOS LG Nexus 5X](https://www.youtube.com/watch?v=H8zAoICS9OY). _These core apps are interesting, sadly they are not optimized on 1440x720px screens at 2x scaling._
* Privacy & Tech Tips: [Privacy: Tor Browser On Pinephone/Pinetab (Arch + Mobian Linux)](https://www.youtube.com/watch?v=opQmphGDkS8), and [Howto: Tor Browser + Icon On Desktop Pinephone/Pinetab (2/2)](https://www.youtube.com/watch?v=vtdtCVL51Bo). _I am really happy to see the [proper Tor Browser](https://sourceforge.net/projects/tor-browser-ports/files/) on the PinePhone, although, dear Tor Project, official ports for ARM64 are well overdue!_
* Sk4zZi0uS: [PinePhone Challenge 3 week Update (With More Chess) - 2021-04-22](https://www.youtube.com/watch?v=O2uZR5KH-gI). _This week it's about running Phosh!_
* Manjaro Linux: [A new Dock for the PinePhone - Expand-X by Beelink!](https://www.youtube.com/watch?v=akDinEPYL2c). _Fancy!_
* Camden Bruce: [Performance of Gentoo Plasma Mobile on the pinephone using SkiffOS](https://www.youtube.com/watch?v=usueljI-gWg). _This is seriously fast, which likely is due to this running a highly optimized Gentoo build._
* Polymath Programming: [PinePhone with Allegro5/LitheFX Demo](https://www.youtube.com/watch?v=5pJt5dQn7-E). _Seems to run well._
* Glenn Hancock: [Replacing Motherboard in the PinePhone Part 2](https://www.youtube.com/watch?v=6PhPcNFsyvo).
* PINE64: [It's Time: InfiniTime 1.0](https://tilvids.com/videos/watch/15301b64-08a5-4071-81be-c7edc70dcf55). _BTW: A fellow site, [FOSSphones.com is doing a PineTime](https://fossphones.com/pinetime-giveaway.html) giveaway if you want one._ 
* Geotechland: [Pinephone BETA Unboxing](https://www.youtube.com/watch?v=0FImdtPILO4). _I bet we will have more of these next week!_
* UBports: [Ubuntu Touch Q&A 99](https://www.youtube.com/watch?v=jxBxhFsGBK8). _Another great Q&A! They promised something great for Q&A 100, so stay tuned for that. News: Fixes to the UBports installer, OTA 17 being delayed from it's May 5th date because of an issue in the Nexus 4 GPU driver, progress reports for Focal (20.04) migration, Debian packaging and Miroil (abstraction layer that allows QtMir and Lomiri to run on Mir 2.0). Also, at 45:20 there is valuable information on the differences between 'kernelupgrade' and 'development' channels on the PinePhone._

#### Jing corner
* Niccolò Ve: [JingPad A1 News - What and When](https://www.youtube.com/watch?v=eas-UFdBxEE). _News about the upcoming crowd-funder._
* JingOS: [Developer Update \| VS Code, ThunderBird Mail ARM test on JingOS ARM](https://www.youtube.com/watch?v=xrJuWA4k9sI). _Just because you can, does not mean you should. I must admit that it's kind of fun to see thunderbird on a phone though._


### Stuff I did

#### Content
* OpenSUSE Plasma Mobile on the PinePhone (April 21st, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/672dde9a-772f-41ad-a09f-5128e235fb89), [Odysee](https://odysee.com/@linmob:3/opensuse-plasma-mobile-on-the-pinephone:6), [YouTube](https://www.youtube.com/watch?v=eCrGi8hsGhY). _I am glad that I managed to make another video. Also: Next week this blog is going to look differently (hopefully better). There's still a lot to do._


#### LINMOBapps

I added three apps to LINMOBapps, so that we have a total of 255 apps right now: Added Khronos (time tracker), Birdie (alarm clock) and PineBattery (battery status utility). [See here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
